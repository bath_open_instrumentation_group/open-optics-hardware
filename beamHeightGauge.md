# Beam Height Gauge

This beam height gauge is for aligning laser beams. The height is set to 75 mm, and the table hole spacing is 25 mm. These values can easily be changed as the design file is parametric.

The M5 screws act as pegs which drop inside the M6 holes of the optical table to allow alignment without having to screw in the gauge.

![](Ims/beamHeightGauge-4139872f.jpg)

## Bill Of Materials

* 1 x 3D printed piece ("`scad/beamHeightGauge.scad`")
* 2 x M5x15 button head screws
* 2 x Cylindrical magnets 5 mm diameter, 5 mm height (For example RS components 792-4552)

## Tools required
* 1.5 or 2mm drill bit
* Simple hand drill or drill chuck
* Screw driver or hex driver for M5 screws
* Superglue

## Modifying the height Gauge and Printing

1. Open `scad/beamHeightGauge.scad` in [OpenSCAD](http://www.openscad.org/).
1. Parameters such as the height of the gauge can be changed. All paramters which are customisable are under `*Parameters to change*/`
1. Preview the changes in OpenSCAD using the preview button.
1. Render the file and then export the model to an STL.
1. Open the STL in the slicer of your choice and print the mount **without support**.

## Assembly

1. Put the drill bit in the the drill or chuck and slowly drill out the centre of the conical hole in the gauge. This hole is sloped at 118 degrees to guide the drill bit.
1. Put a large drop of super glue in each cylindrical tray and place a magnet in each tray. Leave on a magnetic surface for 15 mins to dry. Maybe go have a cup of tea?
1. Screw the M5 screws into the triangular holes from the top. This will probably be quite tight. Make sure the screws ar screwed in straight.
