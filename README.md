
![](Ims/Identity/logo.png)

# Open Optics Hardware
This is a space for simple open hardware to replace expensive hardware used in an optics lab. Some components will be useful because they are cheaper, others because they are customisable.

## Downloads
Download 3D printable .STLs are [automatically generated on GitLab](https://gitlab.com/bath_open_instrumentation_group/open-optics-hardware/pipelines). To download select the icon on the far right hand side.

## Hardware

* [Beam height gauge](beamHeightGauge.md)
* [Motorised mirror mount actuator](motorisedMirrorMountActuator.md)
