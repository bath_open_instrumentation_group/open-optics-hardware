# Motorised Mirror Mount Actuator
This will motorise a ThorLabs mirror mount, the design can probably be adjusted to other mirror mounts too.

## Bill Of Materials

* 1 x 3D printed piece ("`scad/motorisedMirrorMountActuator.scad`")
* 2 x Ruland FCMR19-5-5A (or equivalent) - Aluminium flexible shaft couplers, 5 mm & 5 mm bore. One bore will be modified to be 5.6 mm.
* 2 x M3x6 button head screws
* 1 x M3 washer
* 2 x 28BYJ-48 Stepper motors
* 4 x M4x8 button head screws
* 2 x ThorLabs F25ST100 - 1" 1/4-80 adjuster screws
* 1 x ThorLabs KM100 (or KM100CP/M) -  mirror mount
* 1 x Sangaboard PCB, see below

## Printing

1. Open `scad/motorisedMirrorMountActuator.scad` in [OpenSCAD](http://www.openscad.org/).
1. Render the file and then export the model to an STL.
1. Open the STL in the slicer of your choice and print the mount without support.

## Modifying the shaft couplers [requires a lathe]
Without modification the shaft coupler will not clamp on the 5.6 mm shaft of the 1/4-80 adjuster screw. If you can buy 5mm & 5.6 mm bore shaft couplers you can ship this stage.

1. The Rutland shaft couplers have cap head screws which protrude from the side and interfere with the mount. Replace one of the cap head screws with an M3x6 button head screw. If the screws on your shaft couplers do not protrude, skip this step.
1. From the other end of the shaft coupler, remove the M3 cap screw, place a washer in the slit of the shaft coupler, and replace the cap screw and tighten. This will hold the shaft coupler from fully closing.
1. Place the shaft coupler onto your lathe (using a 19 mm collet if available) so that the end with the washer is facing out for machining. Make sure to grip the final section of the couplers not the flexible piece. Put a 5.6 mm drill bit in the tail stock and drill a 5-8mm deep hole in the end.
1. Remove the coupler from the lathe, and remove the washer from the coupler. The coupler should now clamp on the back of the 1/4-80 adjuster screw.
1. Repeat the above steps for the second shaft coupler.

## PCB
The mirror mount is controlled using a SANGAboard v0.2
[PCB files can be found here](https://github.com/rwb27/openflexure_nano_motor_controller/tree/master/pcb_design). The board and components can be ordered from [kitspace](https://kitspace.org/boards/github.com/rwb27/openflexure_nano_motor_controller/).

Soon version v0.3 should be available for sale.

## Assembly

1. Remove the adjuster screws that come with the mirror mount.
1. Replace these screws with the F25ST100 1/4-80 adjuster screws (without the shaft couplers attached)
1. Take the modified shaft couplers, and clamp the unmodified end  onto the shaft of a 28BYJ-48 stepper motor<br>![](Ims/motorisedMirrorMountActuator-b38d3a57.png)
1. Take the 3D printed piece slot a shaft coupler/motor assembly through one of the two outer holes. The motor lugs should align with triangular holes in printed part, these holes are designed to be easy to screw into. Use two M4 button head screws to screw the motor to the printed part.
1. Repeat the previous step for the other motor.<br>![](Ims/motorisedMirrorMountActuator-4857f1d8.png)
1. Clamp the free ends of the two shaft couplers onto the ends of the adjuster screws on the mirror mount.<br>![](Ims/motorisedMirrorMountActuator-07abed33.png)
1. Connect the motors to the outer two connectors on the Sangaboard

# Software for mirror mount

## Arduino firmware for Nano on Sangaboard
Flash the Arduino firmware onto the Sangaboard using the Arduino IDE, the firmware is available [here](https://github.com/rwb27/openflexure_nano_motor_controller/tree/master/arduino_code)

## Installing the python package

Clone or download the [OpenFlexure nano motor controller repository](https://github.com/rwb27/openflexure_nano_motor_controller)

Navigate in a terminal to the downloaded repository and run:

```
pip install -e .
```

## Controlling the mirror mount

The simple example below shows how to move the motors for the mirror mount

```python
from openflexure_stage import OpenFlexureStage as OFS
import time
# Change /dev/ttyUSB0' to the address of your Arduino. Perhaps COM3 on windows!?
mount = OFS('/dev/ttyUSB0')
mount.move_rel(1000,'x')
time.sleep(1)
mount.move_rel(1000,'z')
time.sleep(1)
mount.move_rel(-1000,'x')
time.sleep(1)
mount.move_rel(-1000,'z')
```
