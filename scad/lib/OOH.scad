/*OOH library
Code by Julian Stirling
Bath Open INstrumentation group

Licence: Cern OHL
*/

module OOHlogo(height)
{
    linear_extrude(height) translate([-17.5,-16]) resize([35,0],auto=true) import("logo.dxf");
}

module OOHlogoSq(height,width=20)
{
    linear_extrude(height) translate([-width/2,-width/2]) resize([width,0],auto=true) import("LogoSquare.dxf");
}
