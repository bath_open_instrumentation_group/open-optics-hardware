/*Mirror mount
Code by Kerri Harrington and Julian Stirling
Bath Open INstrumentation group

Licence: Cern OHL
*/

include <lib/OpenEng.scad>;
include <lib/OOH.scad>;

/*Parameters*/
mount_width = 85;
mount_t = 10; //thickness of mount
thumbscrew_pos = 19.1; //distance from mirror mount thumbscrews to centre of mirror
optic_diameter = 31.8; //hole behind optic
motor_h_sep = 35; //separation of holes for motor screws
shaft_d = 24; //diameter for the holes for shaft couplers
motor_h_offset = 8; //offset of screw holes for the stepper motors
fudge = 0.1;

/*Calculations*/
polycorner = mount_width/2+fudge;

difference()
{
    cube([mount_width,mount_width,mount_t], true);
    //Hole for optic
    cylinder(mount_t+fudge, d1=optic_diameter, d2=optic_diameter,center=true,$fn=40);
    //Hole for beam coupler of motor 1 - inline with mount thumbscrew
    translate([thumbscrew_pos,thumbscrew_pos,0])
    {
        cylinder(mount_t+fudge, d=shaft_d, d=shaft_d,center=true, $fn=40);
        //Holes for stepper motor 1
        translate([-motor_h_sep/2,motor_h_offset,0]){ M4_tap_hole(mount_t+fudge,center=true);}
        translate([ motor_h_sep/2,motor_h_offset,0]){ M4_tap_hole(mount_t+fudge,center=true);}
    }
    
    //Hole for beam coupler of motor 2 - inline with mount thumbscrew
    translate([-(thumbscrew_pos),-(thumbscrew_pos),0])
    {
        cylinder(mount_t+fudge, d=shaft_d, d=shaft_d,center=true, $fn=40);
        //Holes for stepper motor 2
        translate([-motor_h_sep/2,-motor_h_offset,0]){ M4_tap_hole(mount_t+fudge,center=true);}
        translate([ motor_h_sep/2,-motor_h_offset,0]){ M4_tap_hole(mount_t+fudge,center=true);}
    }

    //Remove unwanted material
    reflect([1,-1,0])
    {
        linear_extrude(height=mount_t+fudge,center=true)
        {
            polygon(points=[[fudge,polycorner], [-polycorner, polycorner], [-polycorner, -fudge]]);
        }
        
    }
    translate([26,-3,mount_t/2-.5]){OOHlogoSq(1,18);}
}
