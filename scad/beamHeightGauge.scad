/*Beam Height Gauge
Code by Julian Stirling, Bath Open INstrumentation group

Licence: Cern OHL

Design acknowledgements: Charlotte Parry & Kerri Harrington
*/

include <lib/OpenEng.scad>;
include <lib/OOH.scad>;

/*Parameters to change*/
// between holes sets position of aperture relative to holes on optical table
// true:  aperture is between holes on optical table
// false: aperture is above holes on optical table
betweenHoles = true;
// Turn cross hairs on or off
crossHairs = true;
// The top of the height guage can be thinned which helps visibility of beams above the aperture
// especially for tall users
thinTop = false;
beamHeight=75;
heightAbove=25;
width = 40;
thickness = 10;
topThickness = 4;
baseThickness= 10;
baseWidth = 40;
nomTapD= 4;
holeSep = 25;
//Measured magnet diameter
magDiam=6;
//thickness at the bottom of the magnet tray 
magTrayThickness= 2;
logoDepth = .5;

/*Calculcations*/
totalHeight = beamHeight + heightAbove;
//Ratio of inscribed to excribed circle for a equilateral triagle
//This is used to make triangular holes with an inscribed  circle equal to the tap diameter
triangleInsExs = 2;
holeSize=nomTapD*triangleInsExs;
hole1XPos = betweenHoles ? holeSep/2 : 0;
hole1YPos = (baseWidth-thickness-nomTapD)/2;
hole2XPos = betweenHoles ? -holeSep/2 : 0;
hole2YPos = betweenHoles ? (baseWidth-thickness-nomTapD)/2 : -(baseWidth-thickness-nomTapD)/2;
tray1XPos = holeSep/2;
tray1YPos = -(baseWidth-thickness-magDiam)/2;
tray2XPos = -holeSep/2;
tray2YPos = -(baseWidth-thickness-magDiam)/2;
trayDiam = magDiam+1;
fudge=.01;

/*Build*/

difference()
{
    union()
    {
        translate([0,0,totalHeight/2]){ cube([width,thickness,totalHeight],center=true); }
        translate([0,0,baseThickness/2]){ cube([width,baseWidth,baseThickness],center=true); }
    }
    translate([hole1XPos,hole1YPos,baseThickness/2]){M5_tap_hole(2*baseThickness,center=true);}
    translate([hole2XPos,hole2YPos,baseThickness/2]){M5_tap_hole(2*baseThickness,center=true);;}
    translate([tray1XPos,tray1YPos,thickness/2+magTrayThickness]){cylinder(thickness,d1=trayDiam,d2=trayDiam,center=true,$fn=32);}
    translate([tray2XPos,tray2YPos,thickness/2+magTrayThickness]){cylinder(thickness,d1=trayDiam,d2=trayDiam,center=true,$fn=32);}
    translate([0,-(thickness/2-logoDepth+fudge),3*beamHeight/4]){rotate([90,0,0]){OOHlogo(logoDepth);}}
    translate([0,0,beamHeight])
    {
        union()
        {
            rotate([90,0,0]){crossCone(crossHairs);}
            rotate([-90,0,0]){crossCone(crossHairs);}
        }
    }
    if (thinTop)
    {
        translate([0,(thickness+topThickness)/2,beamHeight+heightAbove/2+3]){cube([width+fudge,thickness,heightAbove],center=true);}
        translate([0,-(thickness+topThickness)/2,beamHeight+heightAbove/2+3]){cube([width+fudge,thickness,heightAbove],center=true);}
    }
}

module crossCone(crossHairs=true)
{
    difference()
    {
        cylinder(thickness,r1=0.01,r2=thickness*tan(59));
        if (crossHairs)
        {
            for (i = [0:3])
            {
                rotate([0,0,i*90]){rotate([0,-59,0]){translate([0,0,2]){cylinder(2*thickness,r1=.8,r2=.8,$fn=3);}}}
            }
        }
    }
}