#!/bin/bash
mkdir -p builds
openscad -o builds/beamHeightGauge.stl scad/beamHeightGauge.scad
openscad -o builds/motorisedMirrorMountActuator.stl scad/motorisedMirrorMountActuator.scad
